import time
import datetime
from sklearn.preprocessing import MinMaxScaler
from hrvanalysis import remove_outliers, interpolate_nan_values, remove_ectopic_beats
from hrvanalysis import extract_features
import os
import glob
import pandas as pd
import matplotlib.pyplot as plt
import difflib
import numpy as np
import helper_functions as hf
from scipy.stats import norm, kurtosis, skew, linregress
import math


'''
Feature Extraction Template for fNIRS Data
'''

def initializer():
    '''
    Relies on the idea that fNIRS files have a txt extension
    '''
    fnirs_extension = 'txt'
    fnirs_files = glob.glob('_all_channels_txt_/*.{}'.format(fnirs_extension))
    
    probemap_extension = 'csv'
    probe_map = glob.glob('_all_probe_maps_csv_/*.{}'.format(probemap_extension))

    performance_extension = 'csv'
    performance_files = glob.glob('performance/*.{}'.format(performance_extension))

    ''' features of interest '''
    features_df = pd.DataFrame(columns = ['id','signal_type', 'channel','trial_idx',
    'mean', 'var', 'peak','min','auc','slope', 'kurt', 'skew' ,'time','state' ,'condition'])

    perf_df = pd.DataFrame(columns = ['delay','std', 'time_stamp'])
    
    return fnirs_files, performance_files, probe_map, features_df, perf_df

'''
Performance Data Modifier
'''

def get_response_data(in_data, rm_out):

    q_low = in_data["response_delay"].quantile(0.1)
    q_hi  = in_data["response_delay"].quantile(0.9)

    if rm_out == True:
        new_df = in_data[(in_data["response_delay"] < q_hi) & (in_data["response_delay"] > q_low)]
    else: 
        new_df = in_data

    new_df = new_df.dropna(subset=['response_delay'])
    new_df = new_df.drop(new_df.index[new_df['incorrect_response']==1])
    
    x = new_df['absolute_response_time']    
    y = new_df['response_delay']
    
    return x,y

'''
Match data files across modes (string comparison)
'''

def get_file_match(f, all_files):
    
    for element in all_files:
        if f[19:-8] == element[12:-4]:
            matched_file = element
    return matched_file


'''
Find fNIRS channel index position, given src, det and signal type

'''
def get_channel_idx(source, detector, probe_map,signal_type):
    
    probe_map_data = pd.read_csv(probe_map, names=["SOURCE", "DETECTOR", "DUMMY1", "DUMMY2"]) 
    source = source.lower()
    source = ord(source)-96
    
    channel_idx =  probe_map_data.loc[(probe_map_data["DETECTOR"] == detector)
     & (probe_map_data["SOURCE"] == source)].index.values[0]
    
    if signal_type == 'HBR':
        channel_idx+=1
    elif signal_type == 'HBT':
        channel_idx+=2

    return channel_idx

'''
Scale input data
'''
def feature_scaling(input, range = (0,1)):
    input = input.values.reshape(-1,1)
    scaler = MinMaxScaler(range)
    input_scaled = scaler.fit_transform(input)
    return input_scaled

'''
Fit spline to data
'''
def fit_reg_spline(input_x, input_y):
    x_range = np.arange(input_x.min(),input_x.max(), 0.5)
    spline_model = hf.get_natural_cubic_spline_model(input_x, 
    input_y, input_x.min(),input_x.max(), n_knots=3)
    y_est = spline_model.predict(x_range)
    return x_range, y_est

'''
Feature Plotter
'''
# TODO: Include option to plot performance
def plot_feature_data(axis, data, label, features_df):
    
    '''
    axis - axis handle
    data - df column
    '''
    q_low = features_df[label].quantile(0.1)
    q_hi  = features_df[label].quantile(0.9)

    features_df = features_df[(features_df[label] < q_hi) & (features_df[label] > q_low)]
    scaled_data = feature_scaling(features_df[label])
    axis.scatter(features_df['time'].values, scaled_data, alpha = 0.5)
    axis.get_yaxis().set_ticks([0,1])
    
    
    x_range_feature, y_est_feature = fit_reg_spline(features_df['time'], scaled_data)
    axis.plot(x_range_feature, y_est_feature, linewidth = '3', label = 'NIRS trend', linestyle = '--',alpha= 0.5)

''' R15 ONLY
Assemble fNIRS data 
- Only applies to R15 Data, need to take condNames to extend to other sources
'''

def assemble_fnirs_data(f, column_of_choice):
    
    fnirs_data = pd.read_csv(f)    
    
    ''' separate elements of interest '''
    
    channel_data = pd.DataFrame()
    channel_data['dc'] = fnirs_data.ix[:,column_of_choice]
    channel_data['time'] = fnirs_data.ix[:,-1]
    
    if len(fnirs_data.columns) == 96:
        ''' Modify this to include other stim-markers 
            Check toCSV.m script in MATLAB for data fornat in f
        '''
        channel_data['fc1'] = fnirs_data.ix[:,-6]
        channel_data['fc2'] = fnirs_data.ix[:, -5]
    elif len(fnirs_data.columns) == 97:
        channel_data['fc1'] = fnirs_data.ix[:,-7]
        channel_data['fc2'] = fnirs_data.ix[:, -6]
    elif len(fnirs_data.columns) == 94: #anomalous files
        channel_data['fc1'] = fnirs_data.ix[:,-3]
        channel_data['fc2'] = fnirs_data.ix[:, -2]
    elif len(fnirs_data.columns) == 93: #anomalous files
        channel_data['fc2'] = fnirs_data.ix[:, -3]
        channel_data['fc1'] = 0
    else:
        print('ERROR in stim marker set')
        input('----')
    
    return channel_data

def assemble_fnirs_tdcs_data(f, column_of_choice):
    
    fnirs_data = pd.read_csv(f)    
    
    ''' separate elements of interest '''
    channel_data = pd.DataFrame()
    channel_data['dc'] = fnirs_data.iloc[:,column_of_choice]
    channel_data['time'] = fnirs_data.iloc[:,-1]
    channel_data['pvt'] = fnirs_data.iloc[:,-2]
    channel_data['baseline'] = fnirs_data.iloc[:, -3]

    return channel_data

'''
Segment data into smaller interval between start, stop
'''

def partition_by_stim(channel_data, start, stop):
    interval = channel_data['time'].between(start, stop, inclusive = True)   
    interval_data = channel_data[interval]
    return interval_data

def extract_fnirs_features(input_data, in_df, del_t, domain):

    ''' set required variables here '''
    end_of_df = False
   
    ''' set filter criteria here '''
    fnirs_data_new = input_data.copy()
    fnirs_data_new = fnirs_data_new.reset_index()

    start = fnirs_data_new['time'][0]
    stop = start+del_t

    state_var = 0
    while not(end_of_df):
        
        if stop > fnirs_data_new['time'].iloc[-1]:
            stop = fnirs_data_new['time'].iloc[-1]
            end_of_df = True
            # print('end-of-df')

        interval = fnirs_data_new['time'].between(start,stop, inclusive = True)

        ''' 
            do analysis on interval data provide time stamp as median 
            - Time domain features (SDNN, pNN50, RMSSD)
            - Frequency domain features (LF, HF, LF/HF)
            - Non-linear domain fe  atures (sd1, sd2, sampen)
        '''  

        interval_conc = fnirs_data_new['dc'][interval]
        interval_time = fnirs_data_new['time'][interval]
       
        if interval_conc.size < 15:
            end_of_df = True
            # print('forced end-of-df')
        else:   
            
            if domain  == 'time':
                
                # get time domain features
                mean_interval = interval_conc.mean()
                peak_interval = interval_conc.max()
                min_interval = interval_conc.min()
                var_interval = (interval_conc.std())**2
                kurt_interval = kurtosis(interval_conc, fisher=True)
                skew_interval = skew(interval_conc)
                auc_interval = np.trapz(interval_conc, interval_time)
                slope_interval,_,_,_,_ = linregress(interval_time, interval_conc)
            
            # get frequency domain features
            # TODO: INCOMPLETE pipeline
            if domain  == 'frequency':
                T = 10.167
                N = len(interval_conc)
                yf = np.fft.rfft(interval_conc)
                xf = np.linspace(0.0, 1.0/(2.0*T), N/2)    
                plt.plot(xf, 2.0/N * np.abs(yf[:N//2]))
                plt.show()
            # get wavelet domain features
                   
            ''' Update DF '''
            in_df = in_df.append({'mean':mean_interval,'peak': peak_interval, 'min': min_interval, 
                'var': var_interval,'auc': auc_interval,'slope': slope_interval,'kurt': kurt_interval,
                 'skew':skew_interval,'state': state_var,'time': interval_time.median()},ignore_index = True)

        start = stop 
        stop = start+del_t
        state_var+=1

    return in_df

''' Below function to transfer complete dfs into other tools '''

def feature_df_bridge(fnirs_files, features_df, channel_list, probe_map, file_path ,signal_type, del_t, domain):
    counter = 0
    # across all participants
    for f in fnirs_files:
        print(counter)
        counter+=1
        print('...working...')
        for channel in channel_list:

            # all signal
            for signal in signal_type:
                
                channel_idx = get_channel_idx(channel[0], int(channel[1:]) , file_path+probe_map[0], signal_type = signal)
                ''' get fnirs data in a nice format '''
                channel_data = assemble_fnirs_data(f, channel_idx)
  
                ''' get stim-times from channel data '''
                stim_fc1 = channel_data.loc[channel_data['fc1']==1]['time'].copy()
                stim_fc1 = stim_fc1.reset_index()
                s1 = stim_fc1['time']

                stim_fc2 = channel_data.loc[channel_data['fc2']==1]['time'].copy()
                stim_fc2 = stim_fc2.reset_index()
                s2 = stim_fc2['time']

                ''' organize stim into df '''
                stim_df = pd.DataFrame()
                stim_df['s1'] = s1
                stim_df['s2'] = s2

                # FC1, FC2
                for column in stim_df:

                    for i in range(0,len(stim_df.index)-1):   

                        if abs(stim_df[column].iloc[i] - stim_df[column].iloc[i+1]) > 25:
                            
                            interval = channel_data['time'].between(stim_df[column].iloc[i], stim_df[column].iloc[i+1])
                            interval_data = channel_data[interval]
                            features_df = extract_fnirs_features(interval_data, features_df, del_t, domain)
        
                            features_df['id'].fillna(f[19:-8], inplace = True)
                            features_df['signal_type'].fillna(signal, inplace = True)
                            features_df['channel'].fillna(channel, inplace = True)
                            features_df['trial_idx'].fillna(i, inplace = True)     
                            
                            #TODO: change according to stim-df column (stress or not)
                            if column == 's1':
                                condition = 'no-stress'
                            else:
                                condition = 'stress'
                            
                            features_df['condition'].fillna(condition, inplace = True)
                        
                        else: 
                            
                            pass


                    if math.isnan(stim_df[column].iloc[-1]):
                        pass
                    else:                      
                        interval = channel_data['time'].between(stim_df[column].iloc[-1], 
                        stim_df[column].iloc[-1]+30)


                        interval_data = channel_data[interval]
                        features_df = extract_fnirs_features(interval_data, features_df, del_t, domain)

                        features_df['id'].fillna(f[19:-8], inplace = True)
                        features_df['signal_type'].fillna(signal, inplace = True)
                        features_df['channel'].fillna(channel, inplace = True)
                        features_df['trial_idx'].fillna(i, inplace = True)     
                        
                        #TODO: change according to stim-df column (stress or not)
                        if column == 's1':
                            condition = 'no-stress'
                        else:
                            condition = 'stress'
                        
                        features_df['condition'].fillna(condition, inplace = True)



    return features_df

def feature_viz(id, channel_list, feature_df, signal_type, modifier):

    if modifier == 'channels':
        fig, axs = plt.subplots(len(channel_list), 5)
        count = 0   

        for channel in channel_list:

            if len(channel_list) == 1:
                axs_out = axs
            else:
                axs_out = axs[count]
            
            filtered_df = feature_df[(feature_df['id'] == id) & (feature_df['channel'] == channel) &  (feature_df['signal_type']==signal_type)]
            means = average_features_to_list(filtered_df, 'mean')
            vars = average_features_to_list(filtered_df, 'var')
            peaks = average_features_to_list(filtered_df, 'peak')
            kurts = average_features_to_list(filtered_df, 'kurt')
            skews = average_features_to_list(filtered_df, 'skew')
            plot_bar_graphs(means, vars, peaks, kurts, skews, channel, signal_type, id, fig, axs_out)
            count+=1
    
    elif modifier == 'signals':
        fig, axs = plt.subplots(len(signal_type), 5)
        count = 0  
    
        for signal in signal_type:
            
            axs_out = axs [count]
            
            filtered_df = feature_df[(feature_df['id'] == id) & (feature_df['channel'] == channel_list[0]) &  (feature_df['signal_type']==signal)]
            means = average_features_to_list(filtered_df, 'mean')
            vars = average_features_to_list(filtered_df, 'var')
            peaks = average_features_to_list(filtered_df, 'peak')
            kurts = average_features_to_list(filtered_df, 'kurt')
            skews = average_features_to_list(filtered_df, 'skew')
            
            plot_bar_graphs(means, vars, peaks, kurts, skews, signal, channel_list[0], id, fig, axs_out)
            count+=1
    
    props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
    cols = ['MEAN', 'VAR', 'MAX', 'KURT', 'SKEW']
    
    for ax, col in zip(axs[0], cols):
        ax.set_title(col, size = 'large',bbox=props)

    plt.show()
    return means, vars, peaks, kurts, skews     

def average_features_to_list(filtered_df, label):
    mean_features = [filtered_df[(filtered_df['condition'] == 'no-stress') & (filtered_df['state'] == 0)][label].mean(),
    filtered_df[(filtered_df['condition'] == 'no-stress') & (filtered_df['state'] == 1)][label].mean(), 
    filtered_df[(filtered_df['condition'] == 'stress') & (filtered_df['state'] == 0)][label].mean(),
    filtered_df[(filtered_df['condition'] == 'stress') & (filtered_df['state'] == 1)][label].mean()]
    return mean_features

def plot_bar_graphs(means,vars, peaks, kurts, skews, row_label, title_label, id, fig, axs):

    objs = [means, vars, peaks, kurts, skews]
        
    i = 0
    
    labels = ['NSC', 'NSR', 'SC', 'SR']
    
    for ax in axs:
        
        # Create bars
        bar_width = 1
        
        # The X position of bars
        r1 = [1,2,3,4]

        ax.bar(r1, objs[i], width = bar_width, color = ('#2ecc71','#27ae60','#e74c3c','#c0392b'), alpha = 0.8)
        i+=1
        
        ax.set_xticks(r1)
        ax.set_xticklabels(labels)
        
        ax.ticklabel_format(axis = 'y', useMathText=True, scilimits = (0,0))

    props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)

    axs[0].set_ylabel(row_label, rotation=0, size='large', labelpad = 1.5,  bbox=props)

    fig.set_figwidth(25.0)
    fig.suptitle('['+id+'] '+'['+title_label+']'+' Averaged features across all trials', fontsize=16)
    
def create_wide_df(complete_df):
    
    ''' create columns channel_signal_feature
    '''
    out_df = pd.DataFrame()

    for ids in complete_df.id.unique():   
        
        wide_df = pd.DataFrame()         
        for channels in complete_df.channel.unique():

            for signals in complete_df.signal_type.unique():
                
                for states in [0, 1]:
                                        
                    if states ==0:
                        st = 'CONT'
                    else:
                        st = 'REST'
                        
                    colname = channels+'_'+signals+'_'+st+'_'
                    
                    wide_df[colname+'MEAN'] = complete_df[
                    (complete_df['id']==ids) & 
                    (complete_df['channel']==channels) & 
                    (complete_df['signal_type']==signals) & 
                    (complete_df['state']==states)]['mean'].values
                    
                    wide_df[colname+'VAR'] = complete_df[
                    (complete_df['id']==ids) & 
                    (complete_df['channel']==channels) &    
                    (complete_df['signal_type']==signals) & 
                    (complete_df['state']==states)]['var'].values

                    wide_df[colname+'PEAK'] = complete_df[
                    (complete_df['id']==ids) & 
                    (complete_df['channel']==channels) & 
                    (complete_df['signal_type']==signals) & 
                    (complete_df['state']==states)]['peak'].values

                    wide_df[colname+'MIN'] = complete_df[
                    (complete_df['id']==ids) & 
                    (complete_df['channel']==channels) & 
                    (complete_df['signal_type']==signals) & 
                    (complete_df['state']==states)]['min'].values

                    wide_df[colname+'AUC'] = complete_df[
                    (complete_df['id']==ids) & 
                    (complete_df['channel']==channels) & 
                    (complete_df['signal_type']==signals) & 
                    (complete_df['state']==states)]['auc'].values

                    wide_df[colname+'SLOPE'] = complete_df[
                    (complete_df['id']==ids) & 
                    (complete_df['channel']==channels) & 
                    (complete_df['signal_type']==signals) & 
                    (complete_df['state']==states)]['slope'].values

                    wide_df[colname+'KURT'] = complete_df[
                    (complete_df['id']==ids) & 
                    (complete_df['channel']==channels) & 
                    (complete_df['signal_type']==signals) & 
                    (complete_df['state']==states)]['kurt'].values

                    wide_df[colname+'SKEW'] = complete_df[
                    (complete_df['id']==ids) & 
                    (complete_df['channel']==channels) & 
                    (complete_df['signal_type']==signals) & 
                    (complete_df['state']==states)]['skew'].values

                   
        wide_df['condition'] = complete_df[
        (complete_df['id']==ids) & 
        (complete_df['channel']==channels) & 
        (complete_df['signal_type']==signals) & 
        (complete_df['state']==states)]['condition'].values
        wide_df['id'] = ids
        # print(wide_df)
        # input('----')
        out_df = out_df.append(wide_df, ignore_index = True)

    
    return out_df

